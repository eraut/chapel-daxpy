use Random;
use Time;

config const N: uint = 4;
config const B: uint = 4;

const NpB = N / B;

const D = {1..N};

var randStream = new owned RandomStream(real,0);

const alpha = randStream.getNext();
var x: [D] real;
var y: [D] real;

randStream.fillRandom(x);
randStream.fillRandom(y);

const start = getCurrentTime(TimeUnits.milliseconds);
coforall tid in 1..B {
  const startidx = (tid-1)*NpB+1;
  for i in startidx..startidx+NpB-1 {
    y[i] = y[i] + alpha*x[i];
  }
}
const end = getCurrentTime(TimeUnits.milliseconds);

writeln("Time: ", end-start);
